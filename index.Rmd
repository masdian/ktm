---
title: "Pendaftaran KTM"
author: "Subbagian Registrasi dan Statistik Untirta"
date: "Diupdate pada: `r format(Sys.Date(), '%d %B %Y')`"
output:
  html_document:
    toc: yes
    toc_depth: '2'
    df_print: paged
    theme: spacelab
    toc_float: no
  pdf_document:
    toc: yes
    toc_depth: '2'
---

<style type="text/css">

body{ /* Normal  */
      font-size: 14px;
  }
td {  /* Table  */
  font-size: 12px;
}
h1.title {
  font-size: 28px;
  color: Maroon;
}
h1 { /* Header 1 */
  font-size: 24px;
  color: Maroon;
  font-weight: bold;
}
h2 { /* Header 2 */
  font-size: 20px;
  color: Maroon;
  font-weight: bold;
}
h3 { /* Header 3 */
  font-size: 16px;
#  font-family: "Times New Roman", Times, serif;
  color: Maroon;
  font-weight: bold;
}
h4 { /* Header 4 */
  font-size: 14px;
  color: Maroon;
  font-weight: bold;
}
code.r{ /* Code block */
    font-size: 12px;
}
pre { /* Code block - determines code spacing between lines */
    font-size: 12px;
}
</style>


Kami mengucapkan selamat datang kepada mahasiswa baru Universitas Sultan Ageng Tirtayasa tahun 2020. Untuk melengkapi identitas saudara sebagai mahasiswa, kami membuka pendaftaran Kartu Tanda Mahasiswa (KTM). Kami telah menyediakan formulir pendaftaran KTM sesuai dengan jalur masuk saudara. 

Sebelum melakukan pengisian formulir, pastikan saudara telah mempersiapkan foto dengan ketentuan sebagai berikut.

1. Laki-laki (jas hitam, baju putih, dasi hitam, background merah, ukuran (4×6))
1. Perempuan (jas hitam/blazer, baju putih, dasi hitam, background merah, ukuran ( 4×6))
1. Perempuan berjilbab (jilbab warna putih, background merah, jas hitam/blazer tanpa dasi, ukuran (4×6))
1. File disimpan dalam format jpg dengan ukuran file : **50 KB – 500 KB**
    - Jika NIM anda adalah **3331201234** maka nama file foto yang harus diunggah adalah **3331201234.jpg** seperti gambar berikut
    
        ![*Penamaan file foto yang akan diunggah*](tes.jpg)
1. Pengisian formulir paling lambat tanggal **15 Oktober 2020**

**Formulir ini hanya bisa diisi menggunakan email dengan domain @untirta.ac.id**. Jika saudara mengalami kesulitan dalam mengisi formulir dari kami, **silakan pelajari langkah-langkahnya pada video berikut**

<div align="center">
   <iframe width="560" height="315" src="https://www.youtube.com/embed/TnH_MEHd1dc" frameborder="0" allowfullscreen>
   </iframe>
</div>

Berikut adalah formulir pendaftaran KTM sesuai dengan jalur masuk masing-masing.

- Jalur SBMPTN Reguler silakan daftar [di sini](https://docs.google.com/forms/d/e/1FAIpQLSeSdL6a6KOkBrnDSlJkvKpu21PIar3sMQJ4L79eOYU5U5Homw/viewform){target="_blank"}
- Jalur SBMPTN KIP silakan daftar [di sini](https://docs.google.com/forms/d/e/1FAIpQLSfkaNLQw2d7Xh3y3xyPm7wdrm4gE_8sKtWWvA2NUSRpLQ4Pcw/viewform){target="_blank"}
- Jalur SMMPTN silakan daftar [di sini](https://docs.google.com/forms/d/e/1FAIpQLScUvpfrmEL5F9AVeBXSqXk9ezUTrztLSs8nk0oCmopbjvUG8w/viewform){target="_blank"}
- Jalur UMM D3, Alih Jenjang, dan ADIK silakan daftar [di sini](https://docs.google.com/forms/d/e/1FAIpQLSeg082LC5iShNzc8XmupZ_mkrisqrNiTkcU4xHGTnWpgiIg0Q/viewform){target="_blank"}

Jika ada yang ingin ditanyakan, silakan hubungi Subbagian Registrasi dan Statistik melalui email **registrasi@untirta.ac.id** di hari dan jam kerja, Senin s/d Jumat pukul 08.00 - 16.00

Berikut adalah mahasiswa baru yang sudah mengisi formulir pendaftaran KTM (**gunakan Search untuk pencarian cepat**)

```{r echo=FALSE, message=FALSE, warning=FALSE}
# `r format(Sys.time(), "%d %B %Y")` pukul `r format(Sys.time(), "%X")`
# format waktu
library(googlesheets4)
gs4_deauth()

## 01 sbmptn reguler
data <- NULL
sbm_reg <- "https://docs.google.com/spreadsheets/d/1LGW6-zgG8rZoDog5KEXwqoE3IFK2aD2O_MW_gJ2JdCQ/edit?usp=sharing"
data1 <- data.frame(read_sheet(sbm_reg))
data1 <- data1[,c(1,2,4,5)]
names(data1) <- c("waktu_daftar", "email", "nama", "no_ktp")
data1 <- unique(data1)
data <- rbind(data, data1)

## 02 sbmptn kip

sbm_kip <- "https://docs.google.com/spreadsheets/d/1BQkSjjr5MqXxkzTEM6Rbpwn0wYLX177xeYdzrMepeD0/edit?usp=sharing"
data1 <- data.frame(read_sheet(sbm_kip))
data1 <- data1[,c(1,2,4,5)]
names(data1) <- c("waktu_daftar", "email", "nama", "no_ktp")
data1 <- unique(data1)
data <- rbind(data, data1)

## 03 smmptn

smm <- "https://docs.google.com/spreadsheets/d/1xF0vL_xCOOyK8zrUWQUQcQy_jncBUiZPLfaa8aYKOAU/edit?usp=sharing"
data1 <- data.frame(read_sheet(smm))
data1 <- data1[,c(1,2,4,5)]
names(data1) <- c("waktu_daftar", "email", "nama", "no_ktp")
data1 <- unique(data1)
data <- rbind(data, data1)

## 04 umm
umm <- 'https://docs.google.com/spreadsheets/d/1PetxRxMiQJ1NI4P_OxJDxCu_ZmXs_1MN6Y-0bQhMKpU/edit?usp=sharing'
data1 <- data.frame(read_sheet(umm))
data1 <- data1[,c(1,2,4,5)]
names(data1) <- c("waktu_daftar", "email", "nama", "no_ktp")
data1 <- unique(data1)
data <- rbind(data, data1)
data <- data[,c(1, 2)]
names(data) <- c("Waktu Pendaftaran", "Email/NIM")
DT::datatable(data,
              rownames = F)#,
              #filter = "top")
```